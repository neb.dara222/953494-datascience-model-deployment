from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Lasso
import pandas as pd
import numpy as np
import xgboost as xgb
import pickle
import os
import string
import bisect
import warnings
import re


class BuildEstimator:
    @staticmethod
    def createBlindTestSamples():

        try:
            os.remove('api/lib/data/fitSample.csv')
            os.remove('api/lib/data/blindedTestSample.csv')
        except OSError:
            pass
        # read data from file named flavors_of_cacao.csv
        # and stored in variable named rawData
        rawData = pd.read_csv('api/lib/data/flavors_of_cacao.csv')
        # drop the column named REF and Specific Bean Origin\nor Bar Name in dataset
        rawData = rawData.drop(
            columns=['REF', 'Specific Bean Origin\nor Bar Name'])
        # take only columns named 'Company', 'ReviewDate', 'CocoaPercent',
        # 'CompanyLocation', 'Rating', 'BeanType', 'BeanOrigin'
        rawData.columns = ['Company', 'ReviewDate', 'CocoaPercent',
                           'CompanyLocation', 'Rating', 'BeanType', 'BeanOrigin']
        # remove rows in Company column contained Parentheses ()
        rawData['Company'] = [x.split('(')[0].strip()
                              for x in rawData['Company']]
        # remove % in ColcoaPercent column and convert to float type
        rawData['CocoaPercent'] = [float(x.split('%')[0].strip())
                                   for x in rawData['CocoaPercent']]
        # remove punctuation from each row of CompanyLocation column
        rawData['CompanyLocation'] = [x.translate(str.maketrans(
            '', '', string.punctuation)) for x in rawData['CompanyLocation']]
        # replace empty rows of BeanType column with nan value
        rawData['BeanType'] = rawData['BeanType'].replace('\xa0', np.nan)
        # filled out NaN/Na value rows of BeanType column with Unknown
        rawData['BeanType'] = rawData['BeanType'].fillna('Unknown')
        # clean each row of BeanType column with expression ';|/|&|\.|,|\('
        rawData['BeanType'] = [re.split(';|/|&|\.|,|\(', x)[0].strip()
                               for x in rawData['BeanType']]
        # replace empty rows of BeanOrigin column with nan value
        rawData['BeanOrigin'] = rawData['BeanOrigin'].replace('\xa0', np.nan)
        # filled out NaN/Na value row of BeanOrigin with Unknown
        rawData['BeanOrigin'] = rawData['BeanOrigin'].fillna('Unknown')
        # clean each row of BeanType column with expression ';|/|&|\.|,|\('
        rawData['BeanOrigin'] = [re.split(
            ';|/|&|\.|,|\(', x)[0].strip() for x in rawData['BeanOrigin']]
        # create a list named categoricalColumns containing
        # 'Company','CompanyLocation', 'BeanType', 'BeanOrigin'
        categoricalColumns = ['Company',
                              'CompanyLocation', 'BeanType', 'BeanOrigin']
        # create an empty dictionary named labelDict
        labelDict = {}
        # iterate each element in categoricalColumns list
        for categoricalColumn in categoricalColumns:
            labelDict[categoricalColumn] = LabelEncoder()
            # Fit label encoder and return encoded labels
            rawData[categoricalColumn] = labelDict[categoricalColumn].fit_transform(
                rawData[categoricalColumn])
            # convert each column to be corresponding lists
            curLbl = labelDict[categoricalColumn].classes_.tolist()
            # if Unknown value of each row of each column is not in curLbl
            if 'Unknown' not in curLbl:
                # insert 'Unknown' in sorted order of curLbl
                bisect.insort_left(curLbl, 'Unknown')
            # assign curLbl back to labelDict, repsectively
            labelDict[categoricalColumn].classes_ = curLbl
        # output path
        leOutput = os.path.join('api/lib/data/labelDict.pickle')
        # open output file
        file = open(leOutput, 'wb')
        # save labelDict as a file named labelDict.pickle'
        pickle.dump(labelDict, file)
        # finished writing, and close file
        file.close()
        # drop Rating column
        X = rawData.drop('Rating', axis=1)
        # select Rating Column and assign it to Y
        Y = rawData['Rating']
        # dynamically take X, Y for training and testing
        # as XFit, XBlindTest, yFit and yBlindTest, respectively
        # 30% of data goes testing and the rest goes training
        XFit, XBindTest, yFit, yBlindTest = train_test_split(
            X, Y, test_size=0.3)
        # create index y and append XFit.columns
        column_head = pd.Index(['y']).append(XFit.columns)
        # create dataframe from yFit and XFit and label their head column
        # corresponding column_head and assign to train variable
        train = pd.DataFrame(np.column_stack(
            [yFit, XFit]), columns=column_head)
        # create dataframe from yBlindTest, XBindTest and label their head column
        # corresponding column_head and assign to blind variable
        blind = pd.DataFrame(np.column_stack(
            [yBlindTest, XBindTest]), columns=column_head)
        # write and save train data as fitSample.csv under the data folder
        train.to_csv('api/lib/data/fitSample.csv', index=False)
        # write and save blind data as blindedTestSample.csv under the data folder
        blind.to_csv('api/lib/data/blindedTestSample.csv', index=False)

    @staticmethod
    def getBestPipeline(X, y):
        # prepare a range of alpha values to test
        search_params = {'alpha': [0.01, 0.05, 0.1, 0.5, 1]}
        search_params = dict(('estimator__'+k, v)
                             for k, v in search_params.items())
        # normalize features
        search_params['normalizer'] = [None, StandardScaler()]
        # featureSelector of dictionary named search_params
        # set the Principal Component Analysis
        # n_components=0.90 means to keep 90% of components
        # svd_solver='full' means to run exact full SVD and
        # select the components by postprocessing
        search_params['featureSelector'] = [
            None, PCA(n_components=0.90, svd_solver='full')]
        # create Pipeline assemble 3 steps that can be cross-validated together
        pipe = Pipeline(
            steps=[('normalizer', None), ('featureSelector', None), ('estimator', Lasso())])
        # Select the optimal percentage of features with grid search
        cv = GridSearchCV(pipe, search_params, cv=10, verbose=0, scoring='neg_mean_squared_error',
                          n_jobs=-1, error_score=0.0)
        # fit the model with X and y which are the method's parameters
        cv.fit(X, y)
        # retrun cv which is set the best parameters
        # for model
        return cv

    @staticmethod
    def createModel():
        # read csv file named fitSample.csv
        fit = pd.read_csv('api/lib/data/fitSample.csv')
        # drop column y in fit and assgined to XFit
        XFit = fit.drop(['y'], axis=1)
        # take y column and assigned to yFit
        yFit = fit['y']
        # read csv file named blindedTestSample.csv and assigned to blindTest
        blindTest = pd.read_csv('api/lib/data/blindedTestSample.csv')
        # drop y column and assigned to XBlindTest
        XBlindTest = blindTest.drop(['y'], axis=1)
        # take y column and asigned to yBlindTest
        yBlindTest = blindTest['y']
        # optimize model by using XFit and yFit to
        # get their best estimator for model
        optimizedModel = BuildEstimator.getBestPipeline(
            XFit, yFit).best_estimator_
        # predict XFit and save in yPredFit
        yPredFit = optimizedModel.predict(XFit)
        # predict XBlindTest and save in yPredTest
        yPredTest = optimizedModel.predict(XBlindTest)
        # calculate prediction MSE of yFit and yPredFit
        fit_score = mean_squared_error(yFit, yPredFit)
        # calculate prediction MSE of yBlindTest and yPredTest
        test_score = mean_squared_error(yBlindTest, yPredTest)
        # print out fit MSE and test MSE
        print("fit mse = %.2f and test mse = %.2f" % (fit_score, test_score))
        # open file named flavors_of_cacao.pickle
        file = open('api/lib/model/flavors_of_cacao.pickle', 'wb')
        # write and save optimize model in flavors_of_cacao.pickle
        pickle.dump(optimizedModel, file)
        # finished and close file
        file.close()


if __name__ == '__main__':
    BuildEstimator.createBlindTestSamples()
    BuildEstimator.createModel()
